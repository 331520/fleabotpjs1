/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fleabotpjs;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author ArbuzovA
 */
public class SupportFactory {

    final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Main.class);

    Connection c = checkConnect();

    //check Connect to database
    public Connection checkConnect() {

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:db/base.db");
        } catch (Exception e) {
            logger.error(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        logger.info("Соединение с базой установлено");
        return c;
    }

    //select groups
    public void selectGroups(List<String> groups) throws SQLException {
        String s = null;
        try {
            Statement stmt = null;
            c = checkConnect();
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("select * from groups where active != 'N'");
            while (rs.next()) {
                String grp_name = rs.getString("group_name");
                String grp_id = rs.getString("group_id");
                String url = rs.getString("url");
                s = grp_name + "=" + grp_id + "=" + url;
                groups.add(s);
            }
            logger.info("Группы выбраны корректно");
            rs.close();
            stmt.close();
            c.close();
            logger.info("Соединение с базой закрыто");
        } catch (SQLException e) {
            logger.error("Ошибка выборки групп: " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    //select accounts
    public void selectAccount(List<String> accounts) throws SQLException {
        String s = null;
        try {
            Statement stmt = null;
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("select * from accounts where active != 'N'");
            //ResultSet rs = stmt.executeQuery("select * from accounts");
            while (rs.next()) {
                int id = rs.getInt("acc_id");
                String acc_name = rs.getString("acc_name");
                String acc_pass = rs.getString("acc_pass");
                String app_id = rs.getString("app_id");
                s = acc_name + ":" + acc_pass + ":" + app_id;
                accounts.add(s);
            }
            logger.info("Выборка аккаунтов закончена успешно");
            rs.close();
            stmt.close();
            c.close();
            logger.info("Соединение с базой закрыто");
        } catch (SQLException e) {
            System.err.println("Выборка аккаунтов закончена с ошибкой: " + e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void createScreen(WebDriver driver, String mode) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd hh-mm-ss-S");
        String date = sdf.format(new Date());

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            // Now you can do whatever you need to do with it, for example copy somewhere
            FileUtils.copyFile(scrFile, new File("screens/" + date + "-" + mode + "-" + "screenshot.png"));
        } catch (Exception ex) {
            Logger.getLogger(SupportFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        logger.info("Был создан скриншот экрана " + date + "-" + mode + "-" + "screenshot.png");
    }
}
