/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fleabotpjs;

import static com.mycompany.fleabotpjs.Main.logger;
import io.github.bonigarcia.wdm.PhantomJsDriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ArbuzovA
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException, SQLException {
        // TODO code application logic here
        System.setProperty("console.encoding", "UTF-8");
        logger.setLevel(Level.ALL);
        logger.info("");
        logger.info("-=*=-");
        logger.info("Main class start");

        String mainUrl = "https://www.facebook.com/";
        String makePhoto = "Y";
        List<String> accounts = new ArrayList<>();
        List<String> groups = new ArrayList<>();
        String[] accParts = null;
        String[] grpUrl;
        List<WebElement> myLots = null;
        List<String> allMyLots = new ArrayList<>();
        WebElement eBody;

        SupportFactory mySupportFactory = new SupportFactory();

        mySupportFactory.selectAccount(accounts);

        for (String account : accounts) {
            logger.info("Выбран аккаунт : " + account);

            accParts = account.split(":");
            logger.info("Почта : " + accParts[0]);
            logger.info("Пароль: " + accParts[1]);
            logger.info("App Id: " + accParts[2]);

            logger.info("Создание экземпляра PhantomJSDriver");
            PhantomJsDriverManager.getInstance().setup();
            //String userAgent = "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2700.0 Iron Safari/537.36";
            String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";

            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
            desiredCapabilities.setJavascriptEnabled(true);
            desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_PAGE_SETTINGS_PREFIX + "userAgent", userAgent);
            PhantomJSDriver driver = new PhantomJSDriver(desiredCapabilities);

            WebDriverWait wait = new WebDriverWait(driver, 50);
            logger.info("manage driver");
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(5, TimeUnit.SECONDS);
            driver.manage().window().maximize();//setSize(new Dimension(1024, 768));

            logger.info("Open " + mainUrl);
            driver.get(mainUrl);

            logger.info("Мы на  : " + driver.getTitle());

            try {

                WebElement emailFiled = driver.findElement(By.name("email"));
                Thread.sleep(1000);
                logger.info("ввод логина : " + accParts[0]);
                emailFiled.sendKeys(accParts[0]);

                WebElement passFiled = driver.findElement(By.name("pass"));
                Thread.sleep(1000);
                logger.info("ввод пароля : " + accParts[1]);
                passFiled.sendKeys(accParts[1]);

                Thread.sleep(1000);

                logger.info("Поля заполнены.");
                if (makePhoto.equals("Y")) {
                    mySupportFactory.createScreen(driver, "submit_fields");
                }

                WebElement okButton = null;
                okButton = driver.findElement(By.xpath("//input[@data-testid='royal_login_button']"));

                wait.until(ExpectedConditions.elementToBeClickable(okButton));
                logger.info("okButton : " + okButton.getText());
                logger.info("Клик по кнопке");
                okButton.click();

                logger.info("Скриним состояние после входа");
                if (makePhoto.equals("Y")) {
                    mySupportFactory.createScreen(driver, "enterfb");
                }

            } catch (Exception e) {
                logger.error("Ошибка входа в ФБ : " + e.getLocalizedMessage());
            }

            try {
                logger.info("Вход выполнен. Выбираем группы.");
                mySupportFactory.selectGroups(groups);

                logger.info("Найдено " + groups.size() + " групп.");
                groups.forEach((group) -> {
                    logger.info("Выбрана группа : " + group);
                });
            } catch (Exception e) {
                logger.error("Ошибка выборки аккаунта : " + e.getLocalizedMessage());
            }

            try {
                logger.info("Поиск объявлений");
                for (String group : groups) {
                    grpUrl = group.split("=");
                    logger.info("Ищем в группе " + grpUrl[0]);
                    logger.info("Ссылка : " + grpUrl[2] + "yourposts/?availability=available&referral_surface=your_posts_unsold_notif");
                    driver.get(grpUrl[2] + "yourposts/?availability=available&referral_surface=your_posts_unsold_notif");
                    eBody = driver.findElement(By.cssSelector("body"));
                    logger.info("Скриним состояние после перехода в группу");
                    if (makePhoto.equals("Y")) {
                        mySupportFactory.createScreen(driver, "transfer_to_grp");
                    }
                    logger.info("Поиск лотов");
                    for (int i = 0; i < 10; i++) {
                        myLots = driver.findElements(By.cssSelector("a[href*='sale_post_id']"));
                        if (myLots.isEmpty()) {
                            logger.info("В группе нету лотов");
                            break;
                        }
                        eBody.sendKeys(Keys.END);
                        Thread.sleep(1000);
                    }
                    logger.info("Найдено " + myLots.size() + " лотов");

                    /*
                    for (WebElement myLot : myLots) {
                        logger.info(myLot.getText().length());
                        logger.info(myLot.getText());
                        logger.info(myLot.getAttribute("href"));
                    }
                    
                     */
                    for (WebElement myLot : myLots) {
                        if (myLot.getText().length() > 0 & !myLot.getAttribute("href").contains("comment_id") & !myLot.getAttribute("href").contains("comment_tracking")) {
                            if (!allMyLots.contains(myLot.getAttribute("href"))) {
                                allMyLots.add(myLot.getAttribute("href"));
                                logger.info("Лот добавлен в общий список - " + myLot.getAttribute("href"));
                            } else {
                                logger.info("Этот лот уже найден, пропускаем - " + myLot.getAttribute("href"));
                            }
                        }
                    }
                    Collections.sort(allMyLots);
                    logger.info("Количество лотов в общем списке :  - " + allMyLots.size());
                }

            } catch (Exception e) {
                logger.error("Ошибка поиска объявления : " + e.getLocalizedMessage());
            }

            //check every lot for date update
            try {
                logger.info("Проверяем дату последнего апа каждого лота");
                for (String myLot : allMyLots) {
                    driver.get(myLot);
                }
            } catch (Exception e) {
                logger.error("Ошибка проверки даты " + e.getLocalizedMessage());
            }

            logger.info("Гасим браузер");
            driver.close();
            driver.quit();

        }

        /*
        logger.trace("Trace Message!");
        logger.debug("Debug Message!");
        logger.info("Info Message!");
        logger.warn("Warn Message!");
        logger.error("Error Message!");
        logger.fatal("Fatal Message!");
         */
    }
}
